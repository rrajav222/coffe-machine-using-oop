from menu import Menu, MenuItem
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine

money_machine = MoneyMachine()
coffee_maker = CoffeeMaker()
menu = Menu()
coffee_machine = True
while coffee_machine:
    user_input = input(f"What would you like to have? { menu.get_items() }? ").lower()
    if user_input == "exit":
        coffee_machine = False
    elif user_input == "report":
        coffee_maker.report()
        money_machine.report()
    elif menu.find_drink(user_input):
        order = menu.find_drink(user_input)
        if coffee_maker.is_resource_sufficient(order) and money_machine.make_payment(order.cost):
            coffee_maker.make_coffee(order)


 